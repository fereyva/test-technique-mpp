import axios from 'axios';
import React from 'react';
import '../styles/Login.css';

interface IAddUserProps {
  addUser: () => void
}

class AddUser extends React.Component<IAddUserProps, any> {
  state = {
    last_name: '',
    first_name: '',
    email: '',
    password: '',
  }

  async addUser(event: any) {
    event.preventDefault();

    const body = {
      last_name: this.state.last_name,
      first_name: this.state.first_name,
      email: this.state.email,
      password: this.state.password,
    }

    try {
      const token = JSON.parse(await localStorage.getItem('token') as any);
      await axios.post(`https://mpp-test-api.herokuapp.com/v1/users`, body, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token
        }
      });

      this.props.addUser();
      return true;
    } catch (e) {
      console.debug(e);
      this.props.addUser();
      return false;
    }
  }

  render() {
    return (
      <>
        <div className="login">
        <h1>Ajout d'un utilisateur</h1>
        <form onSubmit={ (e) => this.addUser(e) }>
          <label>
            <p>Nom</p>
            <input type="text" onChange={ (e) =>  this.setState({ last_name: e.target.value } )} />
          </label>

          <label>
            <p>Prénom</p>
            <input type="text" onChange={ (e) =>  this.setState({ first_name: e.target.value } )} />
          </label>

          <label>
            <p>Email</p>
            <input type="email" onChange={ (e) => this.setState({ email: e.target.value} )} />
          </label>

          <label>
            <p>Mot de passe</p>
            <input type="password" onChange={ (e) => this.setState({ password: e.target.value} )} />
          </label>
          <div>
            <button type="submit">Submit</button>
          </div>
        </form>
      </div>
      </>
    );
  }
}

export default AddUser;
