import { AxiosResponse } from 'axios';
import axios from 'axios';
import React from 'react';
import '../styles/Login.css';

interface IloginProps {
  setToken: (token: string) => void
}

class Login extends React.Component<IloginProps, any> {
  state = {
    email: '',
    password: ''
  }

  async login(event: any) {
    event.preventDefault();

    const body = {
      email: this.state.email,
      password: this.state.password,
    }

    try {
      const response: AxiosResponse = await axios.post(`https://mpp-test-api.herokuapp.com/sign-in`, body, {
        headers: {
          'Content-Type': 'application/json'
        }
      });

      this.props.setToken(response.data.token);
      return true;
    } catch (e) {
      console.debug(e);
      return false;
    }

  }

  render() {
    return (
      <div className="login">
        <h1>Login page</h1>
        <form onSubmit={ (e) => this.login(e) }>
          <label>
            <p>Email</p>
            <input type="text" onChange={ (e) => this.setState({ email: e.target.value} )} />
          </label>
          <label>
            <p>Mot de passe</p>
            <input type="password" onChange={ (e) =>  this.setState({ password: e.target.value } )} />
          </label>
          <div>
            <button type="submit">Submit</button>
          </div>
        </form>
      </div>
    )
  }
}

export default Login;
