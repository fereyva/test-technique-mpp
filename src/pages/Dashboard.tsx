import { Modal } from '@mui/material';
import { Toolbar } from '@mui/material';
import { Button } from '@mui/material';
import { Typography } from '@mui/material';
import { AppBar } from '@mui/material';
import { Box } from '@mui/material';
import { TableBody } from '@mui/material';
import { TableCell } from '@mui/material';
import { TableRow } from '@mui/material';
import { TableHead } from '@mui/material';
import { Paper } from '@mui/material';
import { Table } from '@mui/material';
import { TableContainer } from '@mui/material';
import LogoutIcon from '@mui/icons-material/Logout';
import axios from 'axios';
import { AxiosResponse } from 'axios';
import React from 'react';
import '../styles/Dashboard.css'
import AddUser from '../components/AddUser';

interface IDashboardProps {
  logout: () => void
}

interface User {
  email: string,
  first_name: string,
  last_name: string,
}

class Dashboard extends React.Component<IDashboardProps , { open: boolean, users?: User[] }> {
  state = {
    open: false,
    users: []
  }

  handleOpen() {
    this.setState({ open: true });
  }

  handleClose() {
    this.setState({open: false});
  }

  async componentDidMount() {
    await this.getUserList();
  }

  async getUserList() {
    try {
      const token = JSON.parse(await localStorage.getItem('token') as any);
      const response: AxiosResponse = await axios.get(`https://mpp-test-api.herokuapp.com/v1/users`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token
        }
      });

      this.setState( { users : response.data });
      return true;
    } catch (e) {
      console.debug(e);
      return false;
    }
  }

  render() {
    const { open, users } = this.state;

    return(
      <>
        <Box sx={{ flexGrow: 1 }}>
          <AppBar position="static">
            <Toolbar>
              <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                Dashboard
              </Typography>
              <Button color="inherit" onClick={ () => this.props.logout() }><LogoutIcon /></Button>
            </Toolbar>
          </AppBar>
        </Box>

        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Nom</TableCell>
                <TableCell align="center">Prénom</TableCell>
                <TableCell align="center">Email</TableCell>
                <TableCell align="center">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {users.map((user: User) => (
                <TableRow
                  key={user.last_name}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row" align="center">{user.last_name}</TableCell>
                  <TableCell align="center">{user.first_name}</TableCell>
                  <TableCell align="center">{user.email}</TableCell>
                  <TableCell align="center">
                    <Button >Modifier</Button>
                    <Button>Supprimer</Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>


        <Box sx={{ textAlign: 'center', width: '100%' }}>
          <Button onClick={ () => { this.handleOpen() } } sx={{ marginTop: '40px' }}>
            Ajouter un utilisateur
          </Button>
        </Box>


        <Modal
          open={ open }
          onClose={ () => { this.handleClose() } }
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
        <Box className="modal">
          <AddUser addUser={ () => {
            this.handleClose();
            this.getUserList();
          } }/>
        </Box>
      </Modal>
      </>
    );
  }
}

export default Dashboard;
