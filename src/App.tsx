import { ReactElement } from 'react';
import React from 'react';
import './App.css';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import Dashboard from './pages/Dashboard';
import Login from './pages/Login';

class App extends React.Component<any, any>{
  state = { token: null };

  async componentWillMount() {
    const token = JSON.parse(await localStorage.getItem('token') as any);
    this.setState({ token: token });
  }

  saveToken(token: string) {
    this.setState({ token })
    localStorage.setItem('token', JSON.stringify(token));
  }

  logout() {
    localStorage.removeItem('token');
    this.setState({ token: null })
  }

  render(): ReactElement {
    const { token } = this.state;

    if (!token) {
      return (
        <>
          <Login setToken={ (tkn: string) => {
            this.saveToken(tkn)
          } }/>
        </>
    )
    } else {
      return (
        <BrowserRouter>
          <Routes>
            <Route path="/dashboard" element={<Dashboard logout={ () => { this.logout() }}/>} />
            <Route path="/" element={<Navigate replace to="/dashboard" />} />
          </Routes>
        </BrowserRouter>
      );
    }
  }
}

export default App;
